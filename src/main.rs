extern crate clap;
extern crate pancurses;
extern crate rand;

use std::{
	fs::File,
	io::{self, prelude::*},
	thread,
	time::Duration,
};

use clap::Parser;
use pancurses::{endwin, initscr};
use rand::Rng;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args
{
	/// Filepath for the name list
	#[arg(required = true)]
	file: std::path::PathBuf,
}

fn main()
{
	let Args { file: path } = Args::parse();

	let names: Vec<String> = {
		let file = File::open(path).expect("File couldn't be opened.");
		io::BufReader::new(file)
			.lines()
			.map(|l| l.expect("Couldn't read a line inside file."))
			.collect()
	};

	let from = names.len() * 10;
	let to = names.len() * 100;

	let mut rng = rand::thread_rng();
	let choice = rng.gen_range(from..to) as i32;

	let win = initscr();
	pancurses::curs_set(0);
	let (console_h, console_w) = win.get_max_yx();

	let time = |x: i32| {
		if x < choice - 64
		{
			0.01f64
		}
		else if x < choice - 32
		{
			0.04f64
		}
		else if x < choice - 16
		{
			0.07f64
		}
		else if x < choice - 4
		{
			0.2f64
		}
		else
		{
			0.6f64
		}
	};

	let skip = i32::clamp(choice - 128, 0, choice);

	for (i, name) in (skip..choice).zip(names.iter().cycle().skip(skip as usize))
	{
		let (x, y) = ((console_w - name.chars().count() as i32) / 2, console_h / 2);

		win.clear();
		win.mvprintw(y, x, name);
		win.refresh();

		thread::sleep(Duration::from_secs_f64(time(i as i32)));
	}

	win.getch();
	endwin();
}
